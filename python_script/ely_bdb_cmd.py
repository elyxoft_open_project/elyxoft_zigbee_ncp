# coding: utf-8

import logging

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

class ElyBdbCmd:
    def __init__(self, prompt):
        # save variable
        self.cmd = prompt
        # add command
        ##
        setattr(prompt, "do_bdb_channel", self.do_bdb_channel)
        setattr(prompt, "help_bdb_channel", self.help_bdb_channel)
        ##
        setattr(prompt, "do_bdb_factory_reset", self.do_bdb_factory_reset)
        setattr(prompt, "help_bdb_factory_reset", self.help_bdb_factory_reset)
        ##
        setattr(prompt, "do_bdb_start", self.do_bdb_start)
        setattr(prompt, "help_bdb_start", self.help_bdb_start)
        # subscibe to rx handler
        self.cmd.subscribeRxHandler(self.rxHandler)

    def Close(self):
        print("close ElyBdbCmd.")

    ##
    def do_bdb_channel(self, inp):
        self.cmd.sendMesg("bdb c {}\n".format(inp))
    
    def help_bdb_channel(self):
        print("arg : none | get current zigbee channel\narg : channel | set zigbee channel to use 11-26")

    ##
    def do_bdb_factory_reset(self, inp):
        self.cmd.sendMesg("bdb f\n")
    
    def help_bdb_factory_reset(self):
        print("arg : none | perform factory reset.")

    ##
    def do_bdb_start(self, inp):
        self.cmd.sendMesg("bdb s\n")
    
    def help_bdb_start(self):
        print("arg : none | start commissioning.")

    # receive message handler
    def rxHandler(self, i_msg: str):
        l_process = False
        if i_msg.startswith("bdb"):
            print(i_msg)
            l_process = True

        return l_process