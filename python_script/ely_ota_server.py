# coding: utf-8

import zigpy.ota.image as firmware
from pathlib import Path
import logging
import time
from termcolor import colored

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

OTA_DIR_NAME = 'python_script/ota_firmware/'
#OTA_DIR_NAME = 'ota_firmware/'

def image():
    img = firmware.OTAImage()
    img.header = firmware.OTAImageHeader(
        upgrade_file_id=firmware.OTAImageHeader.MAGIC_VALUE,
        header_version=256,
        header_length=56,
        field_control=0,
        manufacturer_id=9876,
        image_type=123,
        file_version=12345,
        stack_version=2,
        header_string="This is a test header!",
        image_size=56 + 2 + 4 + 4,
    )
    img.subelements = [firmware.SubElement(tag_id=0x0000, data=b"data")]

    return img

class ElyOtaServer:
    def __init__(self, prompt):
        # save variable
        self.cmd = prompt
        # add command
        ## ota_notify
        setattr(prompt, "do_ota_notify", self.do_ota_notify)
        setattr(prompt, "help_ota_notify", self.help_ota_notify)
        ## ota_list
        setattr(prompt, "do_ota_list", self.do_ota_list)
        setattr(prompt, "help_ota_list", self.help_ota_list)
        # subscibe to rx handler
        self.cmd.subscribeRxHandler(self.rxHandler)

    def Close(self):
        print("close ElyOtaServer.")

    def do_ota_notify(self, inp):
        inp_split = inp.split(" ")
        if len(inp_split) >= 2:
            # check file validity, save it for later
            # TODO
            # send zcl frame to target
            # zcl c dest_short_addr dest_ep to_client cluster cmd payload
            self.cmd.sendMesg("zcl c {} {} 01 0019 00 0064\n".format(inp_split[0],inp_split[1]))
    
    def help_ota_notify(self):
        print("addr ep | Send OTA Image Notify, addr: target address, ep: target endpoint.")


    def do_ota_list(self, inp):
        # List all files in directory using pathlib
        basepath = Path(OTA_DIR_NAME)
        files_in_basepath = (entry for entry in basepath.iterdir() if entry.is_file())
        for item in files_in_basepath:
            if item.suffix == '.zigbee':
                try:
                    with open(item, mode="rb") as f:
                        parsed_image, _ = firmware.parse_ota_image(f.read())

                        LOGGER.info(
                            "file %s info: man_id: %s, img_type: %s, version: %s, hw_ver: (%s, %s), OTA string: %s, sub-elemnts number: %s",
                            item,
                            parsed_image.header.manufacturer_id, 
                            parsed_image.header.image_type,
                            parsed_image.header.file_version,
                            parsed_image.header.minimum_hardware_version,
                            parsed_image.header.maximum_hardware_version,
                            parsed_image.header.header_string,
                            len(parsed_image.subelements)
                        )
                        
                        for elm in parsed_image.subelements:
                            
                            LOGGER.info(
                                "--> subelement tag_id: %s, length: %s",
                                elm.tag_id,
                                len(elm.data), # [:elm.data._prefix_length],
                            )  
                               
                            # print("--> subelement tag_id: {}, length: {}".format(elm.tag_id,elm.data._prefix_length))                       
                        

                except (OSError, ValueError):
                    LOGGER.info(
                        "File '%s' doesn't appear to be a OTA image", item, exc_info=True
                    )                            

    def help_ota_list(self, inp):
        print("list zigbee ota file available in ota_firmware directory.")

    def hex_reverse(self, val ):
        rev = bytearray.fromhex(val)
        rev.reverse()
        str_out = ''.join(format(x, '02x') for x in rev)
        return str_out

    # receive message handler
    def rxHandler(self, i_msg: str):
        l_process = False
        if "zcl c" in i_msg:
            # print(colored("Parse incoming zcl cluster message {}".format(i_msg),'blue'))
            # zcl c dst_ep src_addr_type src_addr src_ep cluster_id cmd_id is_man_spe payload
            #  0  1    2        3           4        5          6      7      8         9
            msg_dict = dict()
            msg_split = i_msg.split(" ")
            msg_dict['dst_ep'] = msg_split[2]
            msg_dict['src_addr_type'] = msg_split[3]
            msg_dict['src_addr'] = msg_split[4]
            msg_dict['src_ep'] = msg_split[5]
            msg_dict['cluster_id'] = msg_split[6]
            msg_dict['cmd_id'] = msg_split[7]
            msg_dict['is_man_spe'] = msg_split[8]
            msg_dict['payload'] = msg_split[9]
            
            if (msg_dict['cluster_id']=='0019') and (msg_dict['src_addr_type']=='00'):
                l_process = True
                if msg_dict['cmd_id']=='01':
                    LOGGER.info("<-- OTA Query Next Image Request src_addr: {}, payload: {}".format(msg_dict['src_addr'], msg_dict['payload'] ) )
                    l_field_control = int(self.hex_reverse(msg_dict['payload'][0:2]),16)
                    l_manufacturer_code = int(self.hex_reverse(msg_dict['payload'][2:6]),16)
                    l_image_type = int(self.hex_reverse(msg_dict['payload'][6:10]),16)
                    l_file_version = int(self.hex_reverse(msg_dict['payload'][10:18]),16)
                    
                    # search valid firmware in directory
                    # List all files in directory using pathlib
                    basepath = Path(OTA_DIR_NAME)
                    files_in_basepath = (entry for entry in basepath.iterdir() if entry.is_file())
                    for item in files_in_basepath:
                        if item.suffix == '.zigbee':
                            try:
                                with open(item, mode="rb") as f:
                                    parsed_image, _ = firmware.parse_ota_image(f.read())

                                    LOGGER.info(
                                        "file info, man_id: %s, img_type: %s, version: %s, hw_ver: (%s, %s), OTA string: %s",
                                        parsed_image.header.manufacturer_id, 
                                        parsed_image.header.image_type,
                                        parsed_image.header.file_version,
                                        parsed_image.header.minimum_hardware_version,
                                        parsed_image.header.maximum_hardware_version,
                                        parsed_image.header.header_string,
                                    )

                                    if (parsed_image.header.manufacturer_id==l_manufacturer_code) and (parsed_image.header.image_type==l_image_type):
                                        LOGGER.debug("file found send valid answer")
                                        # zcl c dest_short_addr dest_ep to_client cluster cmd payload
                                        rsp_msg = "zcl c {} {} 01 0019 02 00{}{}{}{}\n".format(
                                                            msg_dict['src_addr'],msg_dict['src_ep'],
                                                            self.hex_reverse(format(parsed_image.header.manufacturer_id,"04X")),
                                                            self.hex_reverse(format(parsed_image.header.image_type,"04X")),
                                                            self.hex_reverse(format(parsed_image.header.file_version,"08X")),
                                                            self.hex_reverse(format(parsed_image.header.image_size,"08X")))
                                        self.cmd.sendMesg(rsp_msg)
                            except (OSError, ValueError):
                                LOGGER.info(
                                    "File '%s' doesn't appear to be a OTA image", item, exc_info=True
                                ) 
                                   
                elif msg_dict['cmd_id']=='03':
                    # LOGGER.info("<-- OTA Image Block Request src_addr: {}, payload: {}".format(msg_dict['src_addr'], msg_dict['payload'] ) )
                    l_field_control = int(self.hex_reverse(msg_dict['payload'][0:2]),16)
                    l_manufacturer_code = int(self.hex_reverse(msg_dict['payload'][2:6]),16)
                    l_image_type = int(self.hex_reverse(msg_dict['payload'][6:10]),16)
                    l_file_version = int(self.hex_reverse(msg_dict['payload'][10:18]),16)
                    l_file_offset = int(self.hex_reverse(msg_dict['payload'][18:26]),16)
                    l_max_data_size = int(self.hex_reverse(msg_dict['payload'][26:28]),16)
                    '''
                    LOGGER.info(
                        "payload detail, field control: %02X, man_code: %s, type: %s, version: %s, offset: %s, max_data_size: %s",
                        l_field_control,
                        l_manufacturer_code,
                        l_image_type,
                        l_file_version,
                        l_file_offset,
                        l_max_data_size
                    )
                    '''

                    # search valid firmware in directory
                    # List all files in directory using pathlib
                    basepath = Path(OTA_DIR_NAME)
                    files_in_basepath = (entry for entry in basepath.iterdir() if entry.is_file())
                    for item in files_in_basepath:
                        if item.suffix == '.zigbee':
                            try:
                                with open(item, mode="rb") as f:
                                    f_data = f.read()
                                    parsed_image, _ = firmware.parse_ota_image(f_data)
                                    '''
                                    LOGGER.info(
                                        "file info, man_id: %s, img_type: %s, version: %s, hw_ver: (%s, %s), OTA string: %s",
                                        parsed_image.header.manufacturer_id, 
                                        parsed_image.header.image_type,
                                        parsed_image.header.file_version,
                                        parsed_image.header.minimum_hardware_version,
                                        parsed_image.header.maximum_hardware_version,
                                        parsed_image.header.header_string,
                                    )
                                    '''

                                    if (parsed_image.header.manufacturer_id==l_manufacturer_code) and (parsed_image.header.image_type==l_image_type):

                                        l_file_data_size = len(f_data) - l_file_offset
                                        l_data_size_to_send = min(l_file_data_size, l_max_data_size)

                                        l_data_chunk = f_data[l_file_offset:l_file_offset+l_data_size_to_send].hex()
                                        
                                        '''
                                        LOGGER.info("image data offset: {}, size: {}".format(
                                                            self.hex_reverse(format(l_file_offset,"08X")),
                                                            l_data_size_to_send
                                                    ))
                                        '''
                                        print("\r<-- OTA Image Block Request src_addr: {}, progress {}".format(msg_dict['src_addr'],round((l_file_offset*100/parsed_image.header.image_size),2)),end=' %')
                                        # zcl c dest_short_addr dest_ep to_client cluster cmd payload
                                        rsp_msg = "zcl c {} {} 01 0019 05 00{}{}{}{}{}{}\n".format(
                                                            msg_dict['src_addr'],msg_dict['src_ep'],
                                                            self.hex_reverse(format(parsed_image.header.manufacturer_id,"04X")),
                                                            self.hex_reverse(format(parsed_image.header.image_type,"04X")),
                                                            self.hex_reverse(format(parsed_image.header.file_version,"08X")),
                                                            self.hex_reverse(format(l_file_offset,"08X")),
                                                            format(l_data_size_to_send,"02X"),
                                                            l_data_chunk )
                                        self.cmd.sendMesg(rsp_msg)
                                        
                                        
                            except (OSError, ValueError):
                                LOGGER.info(
                                    "File '%s' doesn't appear to be a OTA image", item, exc_info=True
                                )       
                                
                elif msg_dict['cmd_id']=='06':
                    LOGGER.info("<-- OTA Upgrade End Request src_addr: {}, payload: {}".format(msg_dict['src_addr'], msg_dict['payload']) )
                    l_status = int(self.hex_reverse(msg_dict['payload'][0:2]),16)
                    l_manufacturer_code = int(self.hex_reverse(msg_dict['payload'][2:6]),16)
                    l_image_type = int(self.hex_reverse(msg_dict['payload'][6:10]),16)
                    l_file_version = int(self.hex_reverse(msg_dict['payload'][10:18]),16)
                    LOGGER.info(
                        "payload detail, status: %02X, man_code: %s, type: %s, version: %s",
                        l_status,
                        l_manufacturer_code,
                        l_image_type,
                        l_file_version,
                    )

                    if 0 == l_status:
                        LOGGER.info("SUCCESS send upgrade now")
                        # zcl c dest_short_addr dest_ep to_client cluster cmd payload
                        rsp_msg = "zcl c {} {} 01 0019 07 {}{}{}0000000003000000\n".format(
                                            msg_dict['src_addr'],msg_dict['src_ep'],
                                            self.hex_reverse(format(l_manufacturer_code,"04X")),
                                            self.hex_reverse(format(l_image_type,"04X")),
                                            self.hex_reverse(format(l_file_version,"08X"))
                                            )
                        
                        self.cmd.sendMesg(rsp_msg)                                                       
                                                  
                else:
                    print(colored("unknonw command : "+msg_dict['cmd_id'],'red'))
                    LOGGER.info(
                        "payload detail, field control: %02X, man_code: %s, type: %s, version: %s",
                        l_field_control,
                        l_manufacturer_code,
                        l_image_type,
                        l_file_version
                    )
                    
        return l_process