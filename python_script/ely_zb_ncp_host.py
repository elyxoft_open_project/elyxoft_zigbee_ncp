# coding: utf-8

import threading
import queue
import os

import termcolor
from termcolor import colored

from cmd import Cmd

from ely_ncp_uart import ElyNcpUart
from ely_ota_server import ElyOtaServer
from ely_bdb_cmd import ElyBdbCmd

class MyPrompt(Cmd):
    prompt = '#: '
    intro = "Welcome to Elyxoft Zigbee NCP Host.\n Type ? to list commands"

    def __init__(self):
        super(MyPrompt, self).__init__()
        # private variables
        self.rx_msg_queue = queue.Queue()
        self.rx_thread = None
        self.uart_cli = None
        self.rx_subscriber = list()
        # init command by family
        self.cmd = list()
        self.cmd.append(ElyOtaServer(self))
        self.cmd.append(ElyBdbCmd(self))
        # enable color for terminal on windows
        os.system('color')
        # print(colored(termcolor.COLORS,'red'))

    ## 
    def do_exit(self, inp):
        self.do_close(inp)
        for cmd in self.cmd:
            cmd.Close()
        print("Bye.")
        return True
    
    def help_exit(self):
        print('exit the application. Shorthand: x q Ctrl-D.')
    
    ## 
    def do_open(self, port):
        print("> Open serial port {}.".format(port))
        # initialize uart
        self.uart_cli = ElyNcpUart(port, self.rx_msg_queue)
        # start thread for process received message
        self.rx_thread = ElyCliThread(self.rx_msg_queue, self.rx_subscriber)
        self.rx_thread.start()
        
    def help_open(self):
        print("open a serial port pass as argument.")

    ##
    def do_close(self, inp):
        print("> Close serial port.")
        # stop rx thread
        if self.rx_thread:
            self.rx_thread.join()
        # close uart
        if self.uart_cli:
            self.uart_cli.Close()

    def help_close(self):
        print("close a serial port previously opened.")
      
    ##
    def do_ncp_start(self, inp):
        inp_split = inp.split(" ")
        if len(inp_split) >= 1:
            if len(inp_split) >= 2:
                channel = inp_split[1]
            else:
                channel = '12'
            print("> Start fresh NCP on {} as ZC on channel {}.".format(inp_split[0],channel))
            self.do_open(inp_split[0])
            self.do_bdb_channel(channel)
            self.do_bdb_start(None)

    def default(self, inp):
        if inp == 'x' or inp == 'q':
            return self.do_exit(inp)
    
        print("Default: {}".format(inp))

    def emptyline(self):
        #to avoid the repetition of the last not empty command
        pass

    def sendMesg(self, i_msg:str):
        if self.uart_cli:
            self.uart_cli.SendMessage(i_msg)
            # print("> {}".format(i_msg).rstrip('\n'), end='\n')
        else:
            print("> sendMesg : ERROR serial port not open.")

    def subscribeRxHandler(self, fct_cb):
        self.rx_subscriber.append(fct_cb)

    def unsubscribeRxHandler(self, fct_cb):
        self.rx_subscriber.remove(fct_cb)

    def get_names(self):
        # This method used to pull in base class attributes
        # at a time dir() didn't do it yet.
        # patch to add attribut set by another class
        return list(self.__dict__.keys()) + dir(self.__class__)        


# threading part
class ElyCliThread(threading.Thread):
    def __init__(self, i_rx_queue, i_rx_subscriber):
        super(ElyCliThread, self).__init__()
        # get share variables
        self.msg_queue = i_rx_queue
        self.subscriber = i_rx_subscriber
        # thread control
        self.alive = threading.Event()
        self.alive.set()

    def run(self):
        while self.alive.is_set():
            try:
                # receive message
                msg = self.msg_queue.get(True,1)
                # call subscriber handler
                is_process = False
                for fct in self.subscriber:
                    is_process |= fct(msg)
                # default print
                if not(is_process):
                    print("< {}".format(msg))
                self.msg_queue.task_done()
                print(colored('#','gray'))
            except:
                continue

    def join(self, timeout=None):
        self.alive.clear()
        threading.Thread.join(self, timeout)

if __name__ == '__main__':
    MyPrompt().cmdloop()