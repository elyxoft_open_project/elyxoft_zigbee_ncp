# coding: utf-8

import serial
import threading
import queue
import logging

class ElyNcpUart:

    def __init__(self, i_port:str, i_rx_queue:queue = None):
        # initialize receiving thread
        if i_rx_queue is None:
            self.rx_queue = queue.Queue()
        else:
            self.rx_queue = i_rx_queue
        self.thread = ElyNcpUartThread(self.rx_queue, i_port)
        self.thread.start()

    def Close(self):
        self.thread.join(2)

    def SendMessage(self, i_data:str):
        self.thread.Write(i_data)

    def GetMessage(self, i_timeout=1):
        out = ["timeout"]
        # wait an answer
        try:
            out = self.rx_queue.get(True,i_timeout)
            self.rx_queue.task_done()
        except:
            pass
        return out        


# threading part
class ElyNcpUartThread(threading.Thread):
    def __init__(self, i_rx_queue, i_port):
        super(ElyNcpUartThread, self).__init__()
        # get share variables
        self.rx_queue = i_rx_queue
        self.port = i_port
        # initialise private variable
        self.data = list()        
        # thread control
        self.alive = threading.Event()
        self.alive.set()
        # open serial port
        # logging.debug("ElyNcpUartThread open port {}".format(self.port))
        self.ser = serial.serial_for_url(self.port, 115200, timeout=1) # Serial, work with tcp serial port like ser2net server
    
    def run(self):
        while self.alive.is_set():
            # receive bytes
            chr = self.ser.read(1)
            if (chr != b'') and (chr != b'\x00'):
                if b'\n' == chr:
                    self.rx_queue.put(b''.join(self.data).decode("ascii").strip())
                    self.data.clear()
                else:
                    self.data.append(chr)

    def Write(self, data:str):
        n = self.ser.write(data.encode('utf-8'))
        # logging.debug("write {} byte(s) on {}: {}".format(n,self.port,data))
        # print(">> write {} byte(s) on {}: {}".format(n,self.port,data))
        self.ser.flush()

    def join(self, timeout=None):
        self.alive.clear()
        threading.Thread.join(self, timeout)
        self.ser.close()


if __name__ == "__main__":
    #---------------
    # only for debug
    #---------------

    cont_loop = True

    # configure logger
    # log format
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
    rootLogger = logging.getLogger()
    # log to file
    fileHandler = logging.FileHandler("{0}/{1}.log".format(".", "log_ElyNcpUart"))
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)
    # log to console
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)    
    # log level
    rootLogger.setLevel(logging.INFO)
    
    # starting
    logging.info("Test ElyNcpUart")   
    
    # Init
    cli = ElyNcpUart("COM17")

    # Loop
    while cont_loop:
        try:
            msg = cli.GetMessage(1)
            if "serial" in msg[0]:
                logging.info(msg)
        except KeyboardInterrupt:
            cont_loop = False
        except:
            continue    

    # close
    logging.info("Close")
    cli.Close()
