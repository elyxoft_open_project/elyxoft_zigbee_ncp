# Elyxoft_Zigbee_NCP


## Description
Outils pour la mise au point de produit zigbee.  
Composé d'un firmware pour les cartes nordic:
- nrf52840-dk
- nrf5340-dk

D'un script python permettant d'automatiser l'envoie et le traitement de la cli avec notemment le support d'un serveur OTA.

## License
Voir le fichier de licence BSD.

## Project status
Projet voué à évoluer en fonction des besoins sans feuille de route prédéfine.

## Guide d'utilisation
Testé avec NCS 2.4.0 

DEKIT nrf5340 : c'est la carte qui va être utilisé pour être mise à jour
1. Récuper l'exemple light_switch (ou tout autre firmware qui supporte le FOTA Zigbee) et y ajouter les configs suivantes : 
\# Zigbee shell
CONFIG_ZIGBEE_SHELL=y
CONFIG_ZIGBEE_SHELL_DEBUG_CMD=y
CONFIG_ZIGBEE_SHELL_ENDPOINT=64
\# Increase RX serial ring buffer
CONFIG_SHELL_BACKEND_SERIAL_RX_RING_BUFFER_SIZE=128
2. Flash le sample "light_switch" avec l'overlay FOTA sur la board nrf5340
3. Ouvrir le port COM, vous pouvez taper "help"
4. Taper "bdb help"
5. Taper ensuite channel pour savoir quel channel est utilisé
6. Taper ensuite "channel 16" pour set le channel à 16
7. Aller dans le dossier "build/zephyr/" pour récupérer le fichier ".zigbee" et le placer dans le dossier "ota_firmware" : c'est le fichier utilisé pour la maj Zigbee FOTA

DEKIT nrf52840 : c'est la carte serveur NCP avec qui le PC va échanger pour piloter la mise à jour
1. Flasher le firmware du NCP sur une devkit nrf53840 (par exemple)
2. Lancer le script de ce repository "ely_zb_ncp_host.py" dans le dossier python_script. Note: vous devrez surement installez des dépendences : pyserial, zigpy... Essayez de l'executer pour voir si ca fonctionne
3. Depuis le script, taper les commandes suivantes:
4. Taper "open COMXX" avec XX le numéro du port COM de votre devkit
5. "bdb_factory_reset" : permet de remettre à défaut les paramètres Zigbee FOTA
6. "bdb_channel 16" : set le channel 16 (le même que sur l'autre devkit)
7. "bdb_start" pour démarrer le réseau. Vérifier avec le RTT si ca a fonctionné. Sinon faite cette commande une 2ème fois.
8. (option) Vérifiez avec Wireshark que vous voyez bien les messages sur le réseau grâce à un sniffer 802.15.4 - Zigbee. Si ce n'est pas le cas vous n'avez pas utiliser le bon port COM, reesayez avec l'autre port COM à l'étape 4.
9. "ota_notify a6b1 5" : cette commande permet de mettre à jour avec le fichier .zigbee que vous avez placé dans le dossier "ota_firmware". 
       Note : 
       - l'adressz a6b1 est obtenu grâce aux RTT (chercher "Device update received") ou au sniffing avec Wireshark (voir colonne "source")
       - Le endpoint "5" est indiqué dans le prj.conf du projet. Il peut aussi être déterminé en regardant avec Wireshark le "endpoint source" dans la partie "data" d'une trame "OTA" (trouvé en recherchant par mot-clef)

-> Vous devriez voir la progression en pourcentage en RTT sur la nrf5340 (et aussi sur Wireshark les trames)

*Configuration Wireshark : 
1. Suivez le tuto de nordic pour utiliser un dongle ou devkit pour sniffer le 802.15.4
2. Aller dans "Edit->Preferences...". Ensuite cliquez sur "Protocols" puis tout en bas sur "Zigbee"
3. A côté de "Pre-configured keys", cliquez sur "Editer... "
4. Ajouter les clefs suivantes : 
5a 69 67 42 65 65 41 6c 6c 69 61 6e 63 65 30 39
d0 d1 d2 d3 d4 d5 d6 d7 d8 d9 da db dc dd de df
