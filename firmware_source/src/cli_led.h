/***
 * ELYXOFT - 2023
*/

#ifndef CLI_LED_H
#define CLI_LED_H


#include "cli.h"

//-------------------------------------
// cli module access
extern sCli_t cli_led;

void cli_led_set( uint8_t value );

#endif // CLI_LED_H