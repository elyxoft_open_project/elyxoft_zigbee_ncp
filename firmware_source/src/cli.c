/***
 * ELYXOFT - 2023
*/


#include <string.h>

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "cli.h"
#include "cli_zcl.h"
#include "cli_bdb.h"
#include "cli_led.h"

LOG_MODULE_REGISTER(cli, LOG_LEVEL_INF);

#define CLI_STACK_SIZE 1024
#define CLI_PRIORITY 5

K_THREAD_STACK_DEFINE(cli_stack_area, CLI_STACK_SIZE);
struct k_thread cli_thread_data;

// available module
static sCli_t *cli_module[] = 
{
  &cli_zcl,
  &cli_bdb,
  &cli_led,
  NULL // NULL Terminated
};

//----------------------------------------------------------------------------------
// PRIVATE FUNCTION
//----------------------------------------------------------------------------------

void cli_thread_entry_point( void *p1, void *p2, void *p3 )
{
    /* init internal */
    uint8_t l_msg[CLI_UART_MAX_MSG_SIZE] = {0};

    while(1)
    {
        /* thread loop */
        k_msgq_get(&cli_uart_rx_q, l_msg, K_FOREVER);

        // search if a module is available for process command
        bool module_found = false;
        uint8_t l_idx = 0;        
        while( !module_found && (NULL != cli_module[l_idx]) )
        {
            if( 0 == strncmp(l_msg, cli_module[l_idx]->name, 3) )
            {
                // search command
                char* saveptr;
                const char *p_tok = strtok_r(l_msg, " ", &saveptr);

                // get command
                p_tok = strtok_r(NULL, " ", &saveptr);

                // process
                if( (NULL != p_tok) && (NULL != cli_module[l_idx]->p_process_table) )
                {
                    uint8_t l_cmd_idx = 0;
                    while( !module_found && (NULL != cli_module[l_idx]->p_process_table[l_cmd_idx].pfct_process) )
                    {
                        if( p_tok[0] == cli_module[l_idx]->p_process_table[l_cmd_idx].cmd )
                        {
                            cli_module[l_idx]->p_process_table[l_cmd_idx].pfct_process(&saveptr);
                            module_found = true;
                        }
                        l_cmd_idx++;
                    }
                }
            }
            l_idx++;
        } 

        if( !module_found )
        {
            LOG_INF("not process: %s",l_msg);
            cli_send("cmd not found !\n");
        }

        k_yield();       
    }
}


//----------------------------------------------------------------------------------
// PUBLIC FUNCTION
//----------------------------------------------------------------------------------

bool cli_init( void )
{
    bool lo_success = true;

    lo_success = cli_uart_init();

    // initialize other module
    uint8_t l_idx = 0;
    while( NULL != cli_module[l_idx] )
    {
        if( NULL != cli_module[l_idx]->pfct_init )
        {
        cli_module[l_idx]->pfct_init();
        }
        l_idx++;
    }    

    if( lo_success )
    {
        k_thread_create(&cli_thread_data, cli_stack_area,
                        K_THREAD_STACK_SIZEOF(cli_stack_area),
                        cli_thread_entry_point,
                        NULL, NULL, NULL,
                        CLI_PRIORITY, 0, K_NO_WAIT);
    }

    return lo_success;
}

/**
 * @brief use to send a message to uart
 * 
 * @param p_msg : shall be a string null terminated
 */
void cli_send( uint8_t *p_msg )
{
    uint8_t l_msg[CLI_UART_MAX_MSG_SIZE] = {0};

    strncpy( l_msg, p_msg, CLI_UART_MAX_MSG_SIZE-1 );

    while (k_msgq_put(&cli_uart_tx_q, l_msg, K_NO_WAIT) != 0) {
        /* message queue is full: purge old data & try again */
        k_msgq_purge(&cli_uart_tx_q);
    }

}