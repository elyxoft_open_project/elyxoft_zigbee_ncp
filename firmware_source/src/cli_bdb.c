/***
 * ELYXOFT - 2023
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "cli_bdb.h"

#include <zboss_api.h>
#include <zb_nrf_platform.h>

LOG_MODULE_REGISTER(cli_bdb, LOG_LEVEL_INF);

//----------------------------------------------------------------------------------
// PRIVATE FUNCTION
//----------------------------------------------------------------------------------

/**
* set/get channel
* bdb c channel : channel in decimal between 11-26
*/
static void cli_bdb_c_process( char **save_ptr_tok )
{
    const char *ip_tok = strtok_r(NULL, " ", save_ptr_tok);
    if( NULL == ip_tok )
    {
        // do a get if possible
        uint8_t tmp_rsp[CLI_UART_MAX_MSG_SIZE] = {0};
        uint32_t l_ch = zb_get_bdb_primary_channel_set();
        snprintf(tmp_rsp,CLI_UART_MAX_MSG_SIZE,"bdb c %08x\n",l_ch);
        cli_send(tmp_rsp);
    }
    else
    {
        /*
        if( zigbee_is_stack_started() )
        {
            LOG_WRN("zigbee stack already start, can not set channel !");
        }
        else
        {
            */
            uint8_t l_channel = (uint8_t)strtol(ip_tok,NULL,10);
            uint32_t channel_mask = 1 << l_channel;

            zb_set_bdb_primary_channel_set(channel_mask);
            zb_set_bdb_secondary_channel_set(channel_mask);
            zb_set_channel_mask(channel_mask);

            LOG_INF("Set channel %d mask %08x",l_channel,channel_mask);
        //}
    }
}

/**
* do factory reset
* bdb f
*/
static void cli_bdb_f_process( char **save_ptr_tok )
{
    LOG_INF("Do factory reset");
    ZB_SCHEDULE_APP_CALLBACK(zb_bdb_reset_via_local_action, 0);
}

/**
* start network steering, form network if not exist
* bdb s
*/
static void cli_bdb_s_process( char **save_ptr_tok )
{
	uint32_t channel;
	zb_uint8_t mode_mask = ZB_BDB_NETWORK_STEERING;

	if ((!zigbee_is_stack_started())) 
    {
		channel = zb_get_bdb_primary_channel_set();

        zb_set_network_coordinator_role(channel);
        LOG_INF("Started coordinator");

		zigbee_enable();
	} 
    else 
    {
		/* Handle case where Zigbee coordinator has left the network
		 * and a new network needs to be formed.
		 */
		if( !ZB_JOINED() ) 
        {
			mode_mask = ZB_BDB_NETWORK_FORMATION;
		}
		bdb_start_top_level_commissioning(mode_mask);
	}
}

//----------------------------------------------------------------------------------
// PUBLIC FUNCTION
//----------------------------------------------------------------------------------

/**
* module process table
*/
sCliProcess_t cli_bdb_process[] = {
  {'c',cli_bdb_c_process},
  {'f',cli_bdb_f_process},
  {'s',cli_bdb_s_process},
  // shall be last line
  CLI_PROCESS_END
};

/**
* module access definition
*/
sCli_t cli_bdb = {
  "bdb",
  NULL,
  cli_bdb_process
};