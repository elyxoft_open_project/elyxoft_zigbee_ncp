/***
 * ELYXOFT - 2023
*/

#ifndef CLI_ZCL_H
#define CLI_ZCL_H


#include "cli.h"

//-------------------------------------
// cli module access
extern sCli_t cli_zcl;

#endif // CLI_ZCL_H