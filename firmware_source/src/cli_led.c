/***
 * ELYXOFT - 2023
*/

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <dk_buttons_and_leds.h>

#include "cli_led.h"

LOG_MODULE_REGISTER(cli_led, LOG_LEVEL_INF);

/* LED used for device identification. */
#ifdef CONFIG_BOARD_NRF52840DONGLE_NRF52840
#define IDENTIFY_LED                     DK_LED1
#else
#define IDENTIFY_LED                     DK_LED4
#endif /* defined CONFIG_BOARD_NRF52840DONGLE_NRF52840 */

//----------------------------------------------------------------------------------
// PRIVATE FUNCTION
//----------------------------------------------------------------------------------

static void cli_led_s_process( char **save_ptr_tok )
{
    dk_set_led_on(IDENTIFY_LED);
}

static void cli_led_r_process( char **save_ptr_tok )
{
    dk_set_led_off(IDENTIFY_LED);
}


//----------------------------------------------------------------------------------
// PUBLIC FUNCTION
//----------------------------------------------------------------------------------

void cli_led_set( uint8_t value )
{
    dk_set_led(IDENTIFY_LED, value);
}

/**
* module process table
*/
sCliProcess_t cli_led_process[] = {
  {'s',cli_led_s_process},
  {'r',cli_led_r_process},
  // shall be last line
  CLI_PROCESS_END
};

/**
* module access definition
*/
sCli_t cli_led = {
  "led",
  NULL,
  cli_led_process
};