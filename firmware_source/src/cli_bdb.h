/***
 * ELYXOFT - 2023
*/

#ifndef CLI_BDB_H
#define CLI_BDB_H


#include "cli.h"

//-------------------------------------
// cli module access
extern sCli_t cli_bdb;

#endif // CLI_BDB_H