/***
 * ELYXOFT - 2023
*/

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/drivers/uart.h>

#include <stdio.h>
#include <string.h>

#include "cli_uart.h"

LOG_MODULE_REGISTER(cli_uart, LOG_LEVEL_WRN);

#ifdef CONFIG_BOARD_NRF52840DONGLE_NRF52840
    const struct device *uart= DEVICE_DT_GET(DT_NODELABEL(cdc_acm_uart));
#else
    const struct device *uart= DEVICE_DT_GET(DT_NODELABEL(uart0));
#endif

/* Tx thread */
#define CLI_UART_TX_STACK_SIZE 1024
#define CLI_UART_TX_PRIORITY 5

K_THREAD_STACK_DEFINE(cli_uart_tx_stack_area, CLI_UART_TX_STACK_SIZE);
struct k_thread cli_uart_tx_thread_data;

/* Declare pipe for send/receive */
#define CLI_UART_NB_MSG_BUFFER  10
K_MSGQ_DEFINE(cli_uart_tx_q, CLI_UART_MAX_MSG_SIZE, CLI_UART_NB_MSG_BUFFER, 4);
K_MSGQ_DEFINE(cli_uart_rx_q, CLI_UART_MAX_MSG_SIZE, CLI_UART_NB_MSG_BUFFER, 4);


//----------------------------------------------------------------------------------
// PRIVATE FUNCTION
//----------------------------------------------------------------------------------

static void cli_uart_rx_isr( const struct device *dev, void *user_data )
{
    static uint8_t l_rx_buffer[CLI_UART_MAX_MSG_SIZE] = {0};
    static uint8_t l_rx_buffer_idx = 0;

    uint8_t l_tmp_buf[CLI_UART_MAX_MSG_SIZE];
    uint8_t l_tmp_len;

    while (uart_irq_update(dev) && uart_irq_is_pending(dev)) 
    {
        if (uart_irq_rx_ready(dev))
        {
            l_tmp_len = uart_fifo_read(dev, l_tmp_buf, CLI_UART_MAX_MSG_SIZE);

            for( uint8_t loop=0; loop<l_tmp_len; loop++ )
            {
                if( '\n' == l_tmp_buf[loop] )
                {
                    l_rx_buffer[l_rx_buffer_idx] = 0x00;
                    k_msgq_put(&cli_uart_rx_q, l_rx_buffer, K_NO_WAIT);
                    l_rx_buffer_idx = 0;
                }
                else
                {
                    l_rx_buffer[l_rx_buffer_idx++] = l_tmp_buf[loop];
                    if( l_rx_buffer_idx >= CLI_UART_MAX_MSG_SIZE ){ l_rx_buffer_idx = 0; }
                }
            }
        }
    }
}

void cli_uart_tx_thread_entry_point( void *p1, void *p2, void *p3 )
{
    /* init internal */
    uint8_t l_msg[CLI_UART_MAX_MSG_SIZE] = {0};
    uint8_t l_msg_len = 0;

    while(1)
    {
        /* thread loop */
        k_msgq_get(&cli_uart_tx_q, l_msg, K_FOREVER);

        LOG_INF("send: %s",l_msg);
        l_msg_len = strlen(l_msg);
        for (uint8_t loop = 0; loop < l_msg_len; loop++) {
            uart_poll_out(uart, l_msg[loop]);
        }

        LOG_INF("done !");

        k_yield();
    }
}

//----------------------------------------------------------------------------------
// PUBLIC FUNCTION
//----------------------------------------------------------------------------------

bool cli_uart_init( void )
{
    bool lo_success = true;

	if (!device_is_ready(uart))
    {
		LOG_ERR("UART device not ready\r\n");
		lo_success = false; ;
	}


    if( lo_success )
    {
        uart_irq_callback_set(uart, cli_uart_rx_isr);
        uart_irq_rx_enable(uart);
    }


    if( lo_success )
    {
        k_thread_create(&cli_uart_tx_thread_data, cli_uart_tx_stack_area,
                        K_THREAD_STACK_SIZEOF(cli_uart_tx_stack_area),
                        cli_uart_tx_thread_entry_point,
                        NULL, NULL, NULL,
                        CLI_UART_TX_PRIORITY, 0, K_NO_WAIT);
    }    

    return lo_success;
}