/***
 * ELYXOFT - 2023
*/

#ifndef CLI_UART_H
#define CLI_UART_H

#include <zephyr/kernel.h>
#include <stdbool.h>

//-----------------------------------------------

#define CLI_UART_MAX_MSG_SIZE   200 // MAX 254

extern struct k_msgq cli_uart_tx_q;
extern struct k_msgq cli_uart_rx_q;

//-----------------------------------------------

bool cli_uart_init( void );


#endif // CLI_UART_H