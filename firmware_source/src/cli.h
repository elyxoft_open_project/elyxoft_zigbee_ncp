/***
 * ELYXOFT - 2023
*/

#ifndef CLI_H
#define CLI_H

#include <stdbool.h>
#include <stddef.h>

#include "cli_uart.h"

//-------------------------------------

/**
* @brief define access to cli module
*/
typedef void (*cli_init_fctn)(void);
typedef void (*cli_process_fctn)(char **save_ptr_tok); // message is passed using strtok

typedef struct {
  char cmd;
  cli_process_fctn pfct_process;
}sCliProcess_t;

#define CLI_PROCESS_END  {0xFF,NULL}

typedef struct {
  char name[4];
  cli_init_fctn pfct_init;
  sCliProcess_t *p_process_table;
}sCli_t;

//-------------------------------------

bool cli_init( void );

/**
 * @brief use to send a message to uart
 * 
 * @param p_msg : shall be a string null terminated
 */
void cli_send( uint8_t *p_msg );


#endif // CLI_H