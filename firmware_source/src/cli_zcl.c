/***
 * ELYXOFT - 2023
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cli_zcl.h"
#include "app_zb_config.h"

#include <zboss_api.h>
#include <zb_nrf_platform.h>
#include <zigbee/zigbee_app_utils.h>

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(cli_zcl, LOG_LEVEL_INF);

#define CLI_ZCL_MSG_TABLE_SIZE          10
#define CLI_ZCL_MAX_ZCL_PAYLOAD_SIZE    ZB_ZCL_HI_WO_IEEE_MAX_PAYLOAD_SIZE // ZB_ZCL_HI_MAX_PAYLOAD_SIZE

typedef struct {
    uint8_t         src_ep;
    uint8_t         dest_ep;
    uint16_t        dest_addr;
    uint16_t        cluster_id;
    uint8_t         cmd_id;
    bool            to_client;
    uint8_t         payload_size;
    uint8_t         payload[CLI_ZCL_MAX_ZCL_PAYLOAD_SIZE];
}sCliZclMsgCtx_t;

static sCliZclMsgCtx_t cli_zcl_msg_tbl[CLI_ZCL_MSG_TABLE_SIZE];

//----------------------------------------------------------------------------------
// PRIVATE FUNCTION
//----------------------------------------------------------------------------------

/*****************************************************************
*  ascii character to hex nybble (case insensitive)             *
*****************************************************************/
uint8_t atoh (uint8_t data)
{ 
  if (data > '9') 
  { 
    data += 9;
  }
  return (data & 0x0F);
}


static zb_int8_t cli_zcl_get_free_row_msg_table(void)
{
    // @todo ajouter un mutex pour éviter l'accès concurrentiel
    zb_int8_t lo_row = -1;

    for (zb_int8_t i = 0; i < CLI_ZCL_MSG_TABLE_SIZE; i++)
    {
        if( 0xFF == cli_zcl_msg_tbl[i].src_ep )
        {
            lo_row = i;
            i = CLI_ZCL_MSG_TABLE_SIZE;
        }
    }

    return lo_row;
}

static void cli_zcl_invalidate_row_msg_table(zb_uint8_t row)
{
    if (row < CLI_ZCL_MSG_TABLE_SIZE)
    {
        cli_zcl_msg_tbl[row].src_ep = 0xFF;
    }
}

static void cli_zcl_internal_send( zb_bufid_t bufid, zb_uint16_t data )
{
    sCliZclMsgCtx_t *p_zb_cmd_ctx = &(cli_zcl_msg_tbl[data]);

  // security check
  if( 0xFF != p_zb_cmd_ctx->src_ep )
  {
    zb_uint8_t* ptr = ZB_ZCL_START_PACKET_REQ(bufid)                                            \
    ZB_ZCL_CONSTRUCT_FRAME_CONTROL(ZB_ZCL_FRAME_TYPE_CLUSTER_SPECIFIC, ZB_ZCL_NOT_MANUFACTURER_SPECIFIC, \
                                p_zb_cmd_ctx->to_client, ZB_ZCL_DISABLE_DEFAULT_RESPONSE), \
                                0, \
    ZB_ZCL_CONSTRUCT_COMMAND_HEADER_REQ(ptr, ZB_ZCL_GET_SEQ_NUM(), p_zb_cmd_ctx->cmd_id);
    if( 0 != p_zb_cmd_ctx->payload_size )
    {
      ZB_ZCL_PACKET_PUT_DATA_N(ptr,p_zb_cmd_ctx->payload,p_zb_cmd_ctx->payload_size);
    }
    zb_addr_u l_dest_addr = {.addr_short=p_zb_cmd_ctx->dest_addr};
    if( RET_OK != zb_zcl_finish_and_send_packet_new( bufid, ptr, &l_dest_addr, ZB_APS_ADDR_MODE_16_ENDP_PRESENT, p_zb_cmd_ctx->dest_ep, p_zb_cmd_ctx->src_ep, ZB_AF_HA_PROFILE_ID, p_zb_cmd_ctx->cluster_id, NULL,
                                                    false, true, 0 )
    )
    {
        LOG_ERR("%s, Send FAILED !", __FUNCTION__);
    }
    else
    {
        LOG_DBG("%s, Send SUCCESS !", __FUNCTION__);
    }

    cli_zcl_invalidate_row_msg_table(data);
  }
}

/**
* send a zcl cluster command
* zcl c dest_short_addr dest_ep to_client cluster cmd payload 
*/
static void cli_zcl_c_process( char **save_ptr_tok )
{
    // check parameter is valid
    bool valid_parameter = false;

    // get row in table
    zb_int8_t table_row = cli_zcl_get_free_row_msg_table();

    if( table_row >= 0 )
    {
        sCliZclMsgCtx_t *p_zb_cmd_ctx = &(cli_zcl_msg_tbl[table_row]);

        const char *p_tok = strtok_r(NULL, " ", save_ptr_tok);
        if( NULL != p_tok )
        {
            // dest address
            p_zb_cmd_ctx->dest_addr = (uint16_t)strtol(p_tok,NULL,16);
            p_tok = strtok_r(NULL, " ",save_ptr_tok);
            if( NULL != p_tok )
            {
                // save destination endpoint
                p_zb_cmd_ctx->dest_ep = (zb_uint8_t)strtol(p_tok,NULL,16);

                p_tok = strtok_r(NULL, " ",save_ptr_tok);
                if( NULL != p_tok )
                {
                    // to client
                    p_zb_cmd_ctx->to_client = (zb_uint8_t)strtol(p_tok,NULL,16);
                    
                    p_tok = strtok_r(NULL, " ",save_ptr_tok);
                    if( NULL != p_tok )
                    {
                        // save cluster id
                        p_zb_cmd_ctx->cluster_id = (zb_uint16_t)strtol(p_tok,NULL,16);

                        p_tok = strtok_r(NULL, " ",save_ptr_tok);
                        if( NULL != p_tok )
                        {
                            // save command id
                            p_zb_cmd_ctx->cmd_id = (zb_uint8_t)strtol(p_tok,NULL,16);

                            p_tok = strtok_r(NULL, " ",save_ptr_tok);
                            if( NULL != p_tok )
                            {
                                // save payload
                                p_zb_cmd_ctx->payload_size = 0;
                                uint8_t l_str_value_size = (uint8_t)strlen(p_tok);
                                if( l_str_value_size < (CLI_ZCL_MAX_ZCL_PAYLOAD_SIZE*2) )
                                {
                                    memset(p_zb_cmd_ctx->payload,0,CLI_ZCL_MAX_ZCL_PAYLOAD_SIZE);
                                    for( uint8_t loop=0; loop<l_str_value_size; loop+=2)
                                    {
                                        p_zb_cmd_ctx->payload[loop/2] = atoh(p_tok[loop])<<4;
                                        p_zb_cmd_ctx->payload[loop/2] += atoh(p_tok[loop+1]);
                                    }
                                    // set payload size
                                    p_zb_cmd_ctx->payload_size = l_str_value_size/2;

                                    valid_parameter = true;
                                }
                            }
                            else
                            {
                                // parameter is valid without payload
                                valid_parameter = true;
                            }
                        }
                    }
                }
            }
        }

        if( valid_parameter )
        {
            LOG_INF("%s, cluster %04X, cmd %02X, payload size %d",__FUNCTION__,p_zb_cmd_ctx->cluster_id,p_zb_cmd_ctx->cmd_id,p_zb_cmd_ctx->payload_size);
            p_zb_cmd_ctx->src_ep = APP_ZIGBEE_ENDPOINT;
            if( RET_OK != zb_buf_get_out_delayed_ext(cli_zcl_internal_send, table_row, 0) )
            {
                LOG_WRN("%s, send failed !",__FUNCTION__);
                cli_zcl_invalidate_row_msg_table(table_row);
            }
        }
    }
}


static zb_uint8_t cli_zcl_ep_handler(zb_bufid_t bufid)
{
    uint8_t lo_status = ZB_FALSE;

    zb_zcl_parsed_hdr_t * p_cmd_info = ZB_BUF_GET_PARAM(bufid, zb_zcl_parsed_hdr_t);

    LOG_DBG("%s src_ep: %02X, det_ep: %02X, cluster_id: %04X, profil_id: %04X", __FUNCTION__,
                    p_cmd_info->addr_data.common_data.src_endpoint, p_cmd_info->addr_data.common_data.dst_endpoint, p_cmd_info->cluster_id, p_cmd_info->profile_id );

    if (p_cmd_info->is_common_command == false)
    {
        // cluster command
        uint8_t tmp_rsp[CLI_UART_MAX_MSG_SIZE] = {0};
        char tmp_field_src_addr[24];
        int bytes_written;

        switch(p_cmd_info->addr_data.common_data.source.addr_type)
        {
        case ZB_ZCL_ADDR_TYPE_SHORT:
            sprintf(tmp_field_src_addr,"%04X",p_cmd_info->addr_data.common_data.source.u.short_addr);
        break;

        case ZB_ZCL_ADDR_TYPE_IEEE_GPD:
        case ZB_ZCL_ADDR_TYPE_IEEE:
            bytes_written = to_hex_str(tmp_field_src_addr, (uint16_t)(sizeof(tmp_field_src_addr)/sizeof(char)), 
                                p_cmd_info->addr_data.common_data.source.u.ieee_addr, sizeof(zb_ieee_addr_t), true);
            if ( bytes_written < 0 )
            {
            sprintf(tmp_field_src_addr, "xxxxxxxxxxxxxxxx");
            LOG_ERR("could not find source address ");
            }
        break;

        case ZB_ZCL_ADDR_TYPE_SRC_ID_GPD:
            sprintf(tmp_field_src_addr,"%08X",p_cmd_info->addr_data.common_data.source.u.src_id);
        break;

        default:
        LOG_ERR("Error: bad source address type: %d ", p_cmd_info->addr_data.common_data.source.addr_type);
        sprintf(tmp_field_src_addr, "xxxx");
        break;
        }

        sprintf( tmp_rsp, "zcl c %02X %02X %s %02X %04X %02X %02X",
                p_cmd_info->addr_data.common_data.dst_endpoint,
                p_cmd_info->addr_data.common_data.source.addr_type, tmp_field_src_addr, 
                p_cmd_info->addr_data.common_data.src_endpoint, 
                p_cmd_info->cluster_id, p_cmd_info->cmd_id, p_cmd_info->is_manuf_specific );

        // recover payload if exist
        char field_payload_cmd_value[CLI_ZCL_MAX_ZCL_PAYLOAD_SIZE *2];
        char *p_field_payload_cmd_value = field_payload_cmd_value;

        zb_uint_t payload_cmd_size = zb_buf_len(bufid);
        const uint8_t *payload_cmd_ptr = ( payload_cmd_size > 0 ) ? (uint8_t*)zb_buf_begin(bufid) : NULL;
        if (payload_cmd_ptr)
        {
            while( payload_cmd_size > 0 )
            {
                sprintf(p_field_payload_cmd_value, "%02X", *payload_cmd_ptr);
                p_field_payload_cmd_value = p_field_payload_cmd_value +2; // +2 because 2 bytes used in char tab for 1 byte in input
                payload_cmd_ptr++;
                payload_cmd_size--;
            }
            strcat( strcat(tmp_rsp, " "), field_payload_cmd_value );
        }

        strcat(tmp_rsp, "\n");           

        cli_send( tmp_rsp );

        // never process by zboss stack
        zb_buf_free(bufid);
        lo_status = ZB_TRUE;
    }

    return lo_status;
}

static void cli_zcl_init( void )
{
    for (zb_int8_t i = 0; i < CLI_ZCL_MSG_TABLE_SIZE; i++)
    {
        cli_zcl_msg_tbl[i].src_ep = 0xFF;
    }

    ZB_AF_SET_ENDPOINT_HANDLER(APP_ZIGBEE_ENDPOINT, cli_zcl_ep_handler);
}

//----------------------------------------------------------------------------------
// PUBLIC FUNCTION
//----------------------------------------------------------------------------------

/**
* module process table
*/
sCliProcess_t cli_zcl_process[] = {
  {'c',cli_zcl_c_process},
  // shall be last line
  CLI_PROCESS_END
};

/**
* module access definition
*/
sCli_t cli_zcl = {
  "zcl",
  cli_zcl_init,
  cli_zcl_process
};